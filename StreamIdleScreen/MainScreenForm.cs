﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WMPLib;
using StreamIdleScreen.Parameters;

namespace StreamIdleScreen
{
    public partial class MainScreenForm : Form
    {
        private WindowsMediaPlayer backgroundPlayer = new WindowsMediaPlayer();

        private MainScreenParams mainScreenParams;
        private ImageResizer imageResizer;

        public MainScreenForm(MainScreenParams mainScreenParams)
        {
            InitializeComponent();

            this.mainScreenParams = mainScreenParams;

            imageResizer = new ImageResizer();

            if (!mainScreenParams.BackgroundImageParams.Disabled)
            {
                BackgroundImage = Image.FromFile(mainScreenParams.BackgroundImageParams.ImagePath);
            }

            if (!mainScreenParams.GameLogoImageParams.Disabled)
            {
                GameLogoPictureBox.Image = imageResizer.FitImageByContainerSize(Image.FromFile(mainScreenParams.GameLogoImageParams.ImagePath), GameLogoPictureBox.Width, GameLogoPictureBox.Height);
            }

            if (!mainScreenParams.StreamerAvatarImageParams.Disabled)
            {
                StreamerAvatarPictureBox.Image = imageResizer.FitImageByContainerSize(Image.FromFile(mainScreenParams.StreamerAvatarImageParams.ImagePath), StreamerAvatarPictureBox.Width, StreamerAvatarPictureBox.Height);
                StreamerAvatarPictureBox.Location = new Point(this.Width - StreamerAvatarPictureBox.Width, this.Height - StreamerAvatarPictureBox.Height);
            }
            else
            {
                StreamerAvatarPictureBox.Visible = false;
            }

            timer1.Start();

            backgroundPlayer.URL = Application.StartupPath + "//Music//terran_1.mp3";
            backgroundPlayer.settings.setMode("loop", true);
            backgroundPlayer.controls.play();

            DescriptionPictureBox.Image = ScreenTextFactory.MakeStaticTextImage(mainScreenParams.CountdownDescriptionParams);
            CountdownPictureBox.Image = ScreenTextFactory.MakeTimerTextImage(mainScreenParams.CountdownTimerParams);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            mainScreenParams.CountdownTimerParams.OneSecondTick();
            CountdownPictureBox.Image = ScreenTextFactory.MakeTimerTextImage(mainScreenParams.CountdownTimerParams);
            if (mainScreenParams.CountdownTimerParams.Time.Minutes == 0 && mainScreenParams.CountdownTimerParams.Time.Seconds == 0)
            {
                StopClock();
                StopMusic();
            }
        }

        private void StreamerAvatarPictureBox_Click(object sender, EventArgs e)
        {
            StopClock();
            StopMusic();
        }

        private void StopClock()
        {
            DescriptionPictureBox.Image = ScreenTextFactory.MakeStaticTextImage(mainScreenParams.StaticDescriptionParams);
            CountdownPictureBox.Visible = false;
            timer1.Stop();
        }

        private void StopMusic()
        {
            backgroundPlayer.settings.setMode("loop", false);
            backgroundPlayer.URL = Application.StartupPath + "//Music//finished.mp3";
            backgroundPlayer.controls.play();
        }

        private void MainScreenForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
