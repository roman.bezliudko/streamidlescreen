﻿namespace StreamIdleScreen
{
    partial class MainScreenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CountdownPictureBox = new System.Windows.Forms.PictureBox();
            this.DescriptionPictureBox = new System.Windows.Forms.PictureBox();
            this.GameLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.StreamerAvatarPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.CountdownPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GameLogoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StreamerAvatarPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CountdownPictureBox
            // 
            this.CountdownPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.CountdownPictureBox.Location = new System.Drawing.Point(12, 512);
            this.CountdownPictureBox.Name = "CountdownPictureBox";
            this.CountdownPictureBox.Size = new System.Drawing.Size(1760, 129);
            this.CountdownPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.CountdownPictureBox.TabIndex = 4;
            this.CountdownPictureBox.TabStop = false;
            // 
            // DescriptionPictureBox
            // 
            this.DescriptionPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.DescriptionPictureBox.Location = new System.Drawing.Point(47, 381);
            this.DescriptionPictureBox.Name = "DescriptionPictureBox";
            this.DescriptionPictureBox.Size = new System.Drawing.Size(1725, 125);
            this.DescriptionPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.DescriptionPictureBox.TabIndex = 3;
            this.DescriptionPictureBox.TabStop = false;
            // 
            // GameLogoPictureBox
            // 
            this.GameLogoPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.GameLogoPictureBox.Location = new System.Drawing.Point(12, 12);
            this.GameLogoPictureBox.Name = "GameLogoPictureBox";
            this.GameLogoPictureBox.Size = new System.Drawing.Size(1760, 235);
            this.GameLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.GameLogoPictureBox.TabIndex = 5;
            this.GameLogoPictureBox.TabStop = false;
            // 
            // StreamerAvatarPictureBox
            // 
            this.StreamerAvatarPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.StreamerAvatarPictureBox.Location = new System.Drawing.Point(1582, 799);
            this.StreamerAvatarPictureBox.Name = "StreamerAvatarPictureBox";
            this.StreamerAvatarPictureBox.Size = new System.Drawing.Size(200, 200);
            this.StreamerAvatarPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.StreamerAvatarPictureBox.TabIndex = 6;
            this.StreamerAvatarPictureBox.TabStop = false;
            this.StreamerAvatarPictureBox.Click += new System.EventHandler(this.StreamerAvatarPictureBox_Click);
            // 
            // MainScreenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1784, 1001);
            this.Controls.Add(this.StreamerAvatarPictureBox);
            this.Controls.Add(this.GameLogoPictureBox);
            this.Controls.Add(this.CountdownPictureBox);
            this.Controls.Add(this.DescriptionPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainScreenForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StreamIdleScreen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainScreenForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.CountdownPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GameLogoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StreamerAvatarPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox DescriptionPictureBox;
        private System.Windows.Forms.PictureBox CountdownPictureBox;
        private System.Windows.Forms.PictureBox GameLogoPictureBox;
        private System.Windows.Forms.PictureBox StreamerAvatarPictureBox;
    }
}

