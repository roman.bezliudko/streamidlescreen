﻿namespace StreamIdleScreen
{
    partial class DialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImagesGroupBox = new System.Windows.Forms.GroupBox();
            this.BackgroundImageCheckBox = new System.Windows.Forms.CheckBox();
            this.GameLogoImageCheckBox = new System.Windows.Forms.CheckBox();
            this.StreamerAvatarImageCheckBox = new System.Windows.Forms.CheckBox();
            this.StreamerAvatarImageLabel = new System.Windows.Forms.Label();
            this.StreamerAvatarImageStatusLabel = new System.Windows.Forms.Label();
            this.BrowseStreamerAvatarImageButton = new System.Windows.Forms.Button();
            this.GameLogoImageLabel = new System.Windows.Forms.Label();
            this.GameLogoImageStatusLabel = new System.Windows.Forms.Label();
            this.BrowseGameLogoImageButton = new System.Windows.Forms.Button();
            this.BackgroundImageLabel = new System.Windows.Forms.Label();
            this.BackgroundImageStatusLabel = new System.Windows.Forms.Label();
            this.BrowseBackgroundImageButton = new System.Windows.Forms.Button();
            this.DescriptionGroupBox = new System.Windows.Forms.GroupBox();
            this.CountdownTimerFontConfigButton = new System.Windows.Forms.Button();
            this.StaticDescriptionFontConfigButton = new System.Windows.Forms.Button();
            this.CountdownDescriptionFontConfigButton = new System.Windows.Forms.Button();
            this.StaticDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.StaticDescriptionLabel = new System.Windows.Forms.Label();
            this.CountdownDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.CountdownTimerTrackbarValueLabel = new System.Windows.Forms.Label();
            this.CountdownDescriptionLabel = new System.Windows.Forms.Label();
            this.CountdownTimerLabel = new System.Windows.Forms.Label();
            this.CountdownTimerTrackbar = new System.Windows.Forms.TrackBar();
            this.StartButton = new System.Windows.Forms.Button();
            this.ResourceOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.ImagePreviewPictureBox = new System.Windows.Forms.PictureBox();
            this.ImagesGroupBox.SuspendLayout();
            this.DescriptionGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CountdownTimerTrackbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagePreviewPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ImagesGroupBox
            // 
            this.ImagesGroupBox.Controls.Add(this.BackgroundImageCheckBox);
            this.ImagesGroupBox.Controls.Add(this.GameLogoImageCheckBox);
            this.ImagesGroupBox.Controls.Add(this.StreamerAvatarImageCheckBox);
            this.ImagesGroupBox.Controls.Add(this.StreamerAvatarImageLabel);
            this.ImagesGroupBox.Controls.Add(this.StreamerAvatarImageStatusLabel);
            this.ImagesGroupBox.Controls.Add(this.BrowseStreamerAvatarImageButton);
            this.ImagesGroupBox.Controls.Add(this.GameLogoImageLabel);
            this.ImagesGroupBox.Controls.Add(this.GameLogoImageStatusLabel);
            this.ImagesGroupBox.Controls.Add(this.BrowseGameLogoImageButton);
            this.ImagesGroupBox.Controls.Add(this.BackgroundImageLabel);
            this.ImagesGroupBox.Controls.Add(this.BackgroundImageStatusLabel);
            this.ImagesGroupBox.Controls.Add(this.BrowseBackgroundImageButton);
            this.ImagesGroupBox.Location = new System.Drawing.Point(12, 12);
            this.ImagesGroupBox.Name = "ImagesGroupBox";
            this.ImagesGroupBox.Size = new System.Drawing.Size(394, 194);
            this.ImagesGroupBox.TabIndex = 0;
            this.ImagesGroupBox.TabStop = false;
            this.ImagesGroupBox.Text = "Зображення";
            // 
            // BackgroundImageCheckBox
            // 
            this.BackgroundImageCheckBox.AutoSize = true;
            this.BackgroundImageCheckBox.Location = new System.Drawing.Point(308, 48);
            this.BackgroundImageCheckBox.Name = "BackgroundImageCheckBox";
            this.BackgroundImageCheckBox.Size = new System.Drawing.Size(75, 17);
            this.BackgroundImageCheckBox.TabIndex = 13;
            this.BackgroundImageCheckBox.Text = "Вимкнути";
            this.BackgroundImageCheckBox.UseVisualStyleBackColor = true;
            this.BackgroundImageCheckBox.CheckedChanged += new System.EventHandler(this.BackgroundImageCheckBox_CheckedChanged);
            // 
            // GameLogoImageCheckBox
            // 
            this.GameLogoImageCheckBox.AutoSize = true;
            this.GameLogoImageCheckBox.Location = new System.Drawing.Point(308, 106);
            this.GameLogoImageCheckBox.Name = "GameLogoImageCheckBox";
            this.GameLogoImageCheckBox.Size = new System.Drawing.Size(75, 17);
            this.GameLogoImageCheckBox.TabIndex = 12;
            this.GameLogoImageCheckBox.Text = "Вимкнути";
            this.GameLogoImageCheckBox.UseVisualStyleBackColor = true;
            this.GameLogoImageCheckBox.CheckedChanged += new System.EventHandler(this.GameLogoImageCheckBox_CheckedChanged);
            // 
            // StreamerAvatarImageCheckBox
            // 
            this.StreamerAvatarImageCheckBox.AutoSize = true;
            this.StreamerAvatarImageCheckBox.Location = new System.Drawing.Point(308, 161);
            this.StreamerAvatarImageCheckBox.Name = "StreamerAvatarImageCheckBox";
            this.StreamerAvatarImageCheckBox.Size = new System.Drawing.Size(75, 17);
            this.StreamerAvatarImageCheckBox.TabIndex = 11;
            this.StreamerAvatarImageCheckBox.Text = "Вимкнути";
            this.StreamerAvatarImageCheckBox.UseVisualStyleBackColor = true;
            this.StreamerAvatarImageCheckBox.CheckedChanged += new System.EventHandler(this.StreamerAvatarImageCheckBox_CheckedChanged);
            // 
            // StreamerAvatarImageLabel
            // 
            this.StreamerAvatarImageLabel.AutoSize = true;
            this.StreamerAvatarImageLabel.Location = new System.Drawing.Point(20, 139);
            this.StreamerAvatarImageLabel.Name = "StreamerAvatarImageLabel";
            this.StreamerAvatarImageLabel.Size = new System.Drawing.Size(91, 13);
            this.StreamerAvatarImageLabel.TabIndex = 10;
            this.StreamerAvatarImageLabel.Text = "Аватар стрімера";
            // 
            // StreamerAvatarImageStatusLabel
            // 
            this.StreamerAvatarImageStatusLabel.Location = new System.Drawing.Point(153, 142);
            this.StreamerAvatarImageStatusLabel.Name = "StreamerAvatarImageStatusLabel";
            this.StreamerAvatarImageStatusLabel.Size = new System.Drawing.Size(144, 36);
            this.StreamerAvatarImageStatusLabel.TabIndex = 9;
            // 
            // BrowseStreamerAvatarImageButton
            // 
            this.BrowseStreamerAvatarImageButton.Location = new System.Drawing.Point(23, 155);
            this.BrowseStreamerAvatarImageButton.Name = "BrowseStreamerAvatarImageButton";
            this.BrowseStreamerAvatarImageButton.Size = new System.Drawing.Size(112, 23);
            this.BrowseStreamerAvatarImageButton.TabIndex = 8;
            this.BrowseStreamerAvatarImageButton.Text = "Вказати...";
            this.BrowseStreamerAvatarImageButton.UseVisualStyleBackColor = true;
            this.BrowseStreamerAvatarImageButton.Click += new System.EventHandler(this.BrowseStreamerAvatarImageButton_Click);
            // 
            // GameLogoImageLabel
            // 
            this.GameLogoImageLabel.AutoSize = true;
            this.GameLogoImageLabel.Location = new System.Drawing.Point(20, 84);
            this.GameLogoImageLabel.Name = "GameLogoImageLabel";
            this.GameLogoImageLabel.Size = new System.Drawing.Size(69, 13);
            this.GameLogoImageLabel.TabIndex = 7;
            this.GameLogoImageLabel.Text = "Логотип гри";
            // 
            // GameLogoImageStatusLabel
            // 
            this.GameLogoImageStatusLabel.Location = new System.Drawing.Point(153, 87);
            this.GameLogoImageStatusLabel.Name = "GameLogoImageStatusLabel";
            this.GameLogoImageStatusLabel.Size = new System.Drawing.Size(144, 36);
            this.GameLogoImageStatusLabel.TabIndex = 6;
            // 
            // BrowseGameLogoImageButton
            // 
            this.BrowseGameLogoImageButton.Location = new System.Drawing.Point(23, 100);
            this.BrowseGameLogoImageButton.Name = "BrowseGameLogoImageButton";
            this.BrowseGameLogoImageButton.Size = new System.Drawing.Size(112, 23);
            this.BrowseGameLogoImageButton.TabIndex = 5;
            this.BrowseGameLogoImageButton.Text = "Вказати...";
            this.BrowseGameLogoImageButton.UseVisualStyleBackColor = true;
            this.BrowseGameLogoImageButton.Click += new System.EventHandler(this.BrowseGameLogoImageButton_Click);
            // 
            // BackgroundImageLabel
            // 
            this.BackgroundImageLabel.AutoSize = true;
            this.BackgroundImageLabel.Location = new System.Drawing.Point(20, 26);
            this.BackgroundImageLabel.Name = "BackgroundImageLabel";
            this.BackgroundImageLabel.Size = new System.Drawing.Size(113, 13);
            this.BackgroundImageLabel.TabIndex = 4;
            this.BackgroundImageLabel.Text = "Фонове зображення";
            // 
            // BackgroundImageStatusLabel
            // 
            this.BackgroundImageStatusLabel.Location = new System.Drawing.Point(153, 29);
            this.BackgroundImageStatusLabel.Name = "BackgroundImageStatusLabel";
            this.BackgroundImageStatusLabel.Size = new System.Drawing.Size(144, 36);
            this.BackgroundImageStatusLabel.TabIndex = 3;
            // 
            // BrowseBackgroundImageButton
            // 
            this.BrowseBackgroundImageButton.Location = new System.Drawing.Point(23, 42);
            this.BrowseBackgroundImageButton.Name = "BrowseBackgroundImageButton";
            this.BrowseBackgroundImageButton.Size = new System.Drawing.Size(112, 23);
            this.BrowseBackgroundImageButton.TabIndex = 1;
            this.BrowseBackgroundImageButton.Text = "Вказати...";
            this.BrowseBackgroundImageButton.UseVisualStyleBackColor = true;
            this.BrowseBackgroundImageButton.Click += new System.EventHandler(this.BrowseBackgroundImageButton_Click);
            // 
            // DescriptionGroupBox
            // 
            this.DescriptionGroupBox.Controls.Add(this.CountdownTimerFontConfigButton);
            this.DescriptionGroupBox.Controls.Add(this.StaticDescriptionFontConfigButton);
            this.DescriptionGroupBox.Controls.Add(this.CountdownDescriptionFontConfigButton);
            this.DescriptionGroupBox.Controls.Add(this.StaticDescriptionTextBox);
            this.DescriptionGroupBox.Controls.Add(this.StaticDescriptionLabel);
            this.DescriptionGroupBox.Controls.Add(this.CountdownDescriptionTextBox);
            this.DescriptionGroupBox.Controls.Add(this.CountdownTimerTrackbarValueLabel);
            this.DescriptionGroupBox.Controls.Add(this.CountdownDescriptionLabel);
            this.DescriptionGroupBox.Controls.Add(this.CountdownTimerLabel);
            this.DescriptionGroupBox.Controls.Add(this.CountdownTimerTrackbar);
            this.DescriptionGroupBox.Location = new System.Drawing.Point(12, 212);
            this.DescriptionGroupBox.Name = "DescriptionGroupBox";
            this.DescriptionGroupBox.Size = new System.Drawing.Size(702, 183);
            this.DescriptionGroupBox.TabIndex = 1;
            this.DescriptionGroupBox.TabStop = false;
            this.DescriptionGroupBox.Text = "Текст";
            // 
            // CountdownTimerFontConfigButton
            // 
            this.CountdownTimerFontConfigButton.Location = new System.Drawing.Point(518, 65);
            this.CountdownTimerFontConfigButton.Name = "CountdownTimerFontConfigButton";
            this.CountdownTimerFontConfigButton.Size = new System.Drawing.Size(75, 23);
            this.CountdownTimerFontConfigButton.TabIndex = 8;
            this.CountdownTimerFontConfigButton.Text = "Шрифт";
            this.CountdownTimerFontConfigButton.UseVisualStyleBackColor = true;
            this.CountdownTimerFontConfigButton.Click += new System.EventHandler(this.CountdownTimerFontConfigButton_Click);
            // 
            // StaticDescriptionFontConfigButton
            // 
            this.StaticDescriptionFontConfigButton.Location = new System.Drawing.Point(518, 109);
            this.StaticDescriptionFontConfigButton.Name = "StaticDescriptionFontConfigButton";
            this.StaticDescriptionFontConfigButton.Size = new System.Drawing.Size(75, 23);
            this.StaticDescriptionFontConfigButton.TabIndex = 7;
            this.StaticDescriptionFontConfigButton.Text = "Шрифт";
            this.StaticDescriptionFontConfigButton.UseVisualStyleBackColor = true;
            this.StaticDescriptionFontConfigButton.Click += new System.EventHandler(this.StaticDescriptionFontConfigButton_Click);
            // 
            // CountdownDescriptionFontConfigButton
            // 
            this.CountdownDescriptionFontConfigButton.Location = new System.Drawing.Point(518, 26);
            this.CountdownDescriptionFontConfigButton.Name = "CountdownDescriptionFontConfigButton";
            this.CountdownDescriptionFontConfigButton.Size = new System.Drawing.Size(75, 23);
            this.CountdownDescriptionFontConfigButton.TabIndex = 6;
            this.CountdownDescriptionFontConfigButton.Text = "Шрифт";
            this.CountdownDescriptionFontConfigButton.UseVisualStyleBackColor = true;
            this.CountdownDescriptionFontConfigButton.Click += new System.EventHandler(this.CountdownDescriptionFontConfigButton_Click);
            // 
            // StaticDescriptionTextBox
            // 
            this.StaticDescriptionTextBox.Location = new System.Drawing.Point(223, 111);
            this.StaticDescriptionTextBox.Name = "StaticDescriptionTextBox";
            this.StaticDescriptionTextBox.Size = new System.Drawing.Size(261, 20);
            this.StaticDescriptionTextBox.TabIndex = 6;
            this.StaticDescriptionTextBox.Text = "Стрім починається...";
            // 
            // StaticDescriptionLabel
            // 
            this.StaticDescriptionLabel.AutoSize = true;
            this.StaticDescriptionLabel.Location = new System.Drawing.Point(13, 114);
            this.StaticDescriptionLabel.Name = "StaticDescriptionLabel";
            this.StaticDescriptionLabel.Size = new System.Drawing.Size(147, 13);
            this.StaticDescriptionLabel.TabIndex = 5;
            this.StaticDescriptionLabel.Text = "Верхній текст (після відліку)";
            // 
            // CountdownDescriptionTextBox
            // 
            this.CountdownDescriptionTextBox.Location = new System.Drawing.Point(223, 28);
            this.CountdownDescriptionTextBox.Name = "CountdownDescriptionTextBox";
            this.CountdownDescriptionTextBox.Size = new System.Drawing.Size(261, 20);
            this.CountdownDescriptionTextBox.TabIndex = 1;
            this.CountdownDescriptionTextBox.Text = "Стрім розпочнеться через";
            // 
            // CountdownTimerTrackbarValueLabel
            // 
            this.CountdownTimerTrackbarValueLabel.Location = new System.Drawing.Point(223, 80);
            this.CountdownTimerTrackbarValueLabel.Name = "CountdownTimerTrackbarValueLabel";
            this.CountdownTimerTrackbarValueLabel.Size = new System.Drawing.Size(223, 20);
            this.CountdownTimerTrackbarValueLabel.TabIndex = 4;
            this.CountdownTimerTrackbarValueLabel.Text = "1";
            this.CountdownTimerTrackbarValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CountdownDescriptionLabel
            // 
            this.CountdownDescriptionLabel.AutoSize = true;
            this.CountdownDescriptionLabel.Location = new System.Drawing.Point(13, 31);
            this.CountdownDescriptionLabel.Name = "CountdownDescriptionLabel";
            this.CountdownDescriptionLabel.Size = new System.Drawing.Size(76, 13);
            this.CountdownDescriptionLabel.TabIndex = 0;
            this.CountdownDescriptionLabel.Text = "Верхній текст";
            // 
            // CountdownTimerLabel
            // 
            this.CountdownTimerLabel.AutoSize = true;
            this.CountdownTimerLabel.Location = new System.Drawing.Point(13, 70);
            this.CountdownTimerLabel.Name = "CountdownTimerLabel";
            this.CountdownTimerLabel.Size = new System.Drawing.Size(194, 13);
            this.CountdownTimerLabel.TabIndex = 2;
            this.CountdownTimerLabel.Text = "Час зворотнього відліку (в хвилинах)";
            // 
            // CountdownTimerTrackbar
            // 
            this.CountdownTimerTrackbar.Location = new System.Drawing.Point(223, 55);
            this.CountdownTimerTrackbar.Maximum = 30;
            this.CountdownTimerTrackbar.Minimum = 1;
            this.CountdownTimerTrackbar.Name = "CountdownTimerTrackbar";
            this.CountdownTimerTrackbar.Size = new System.Drawing.Size(223, 45);
            this.CountdownTimerTrackbar.TabIndex = 3;
            this.CountdownTimerTrackbar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.CountdownTimerTrackbar.Value = 1;
            this.CountdownTimerTrackbar.Scroll += new System.EventHandler(this.CountdownTrackbar_Scroll);
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(474, 415);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(240, 23);
            this.StartButton.TabIndex = 2;
            this.StartButton.Text = "Пуск";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // ResourceOpenFileDialog
            // 
            this.ResourceOpenFileDialog.FileName = "openFileDialog1";
            // 
            // ImagePreviewPictureBox
            // 
            this.ImagePreviewPictureBox.Location = new System.Drawing.Point(422, 14);
            this.ImagePreviewPictureBox.Name = "ImagePreviewPictureBox";
            this.ImagePreviewPictureBox.Size = new System.Drawing.Size(292, 192);
            this.ImagePreviewPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ImagePreviewPictureBox.TabIndex = 3;
            this.ImagePreviewPictureBox.TabStop = false;
            // 
            // DialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 460);
            this.Controls.Add(this.ImagePreviewPictureBox);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.DescriptionGroupBox);
            this.Controls.Add(this.ImagesGroupBox);
            this.Name = "DialogForm";
            this.Text = "StreamIdleScreen";
            this.ImagesGroupBox.ResumeLayout(false);
            this.ImagesGroupBox.PerformLayout();
            this.DescriptionGroupBox.ResumeLayout(false);
            this.DescriptionGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CountdownTimerTrackbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagePreviewPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox ImagesGroupBox;
        private System.Windows.Forms.Button BrowseBackgroundImageButton;
        private System.Windows.Forms.GroupBox DescriptionGroupBox;
        private System.Windows.Forms.TrackBar CountdownTimerTrackbar;
        private System.Windows.Forms.Label CountdownTimerLabel;
        private System.Windows.Forms.TextBox CountdownDescriptionTextBox;
        private System.Windows.Forms.Label CountdownDescriptionLabel;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.OpenFileDialog ResourceOpenFileDialog;
        private System.Windows.Forms.PictureBox ImagePreviewPictureBox;
        private System.Windows.Forms.Label BackgroundImageStatusLabel;
        private System.Windows.Forms.Label CountdownTimerTrackbarValueLabel;
        private System.Windows.Forms.TextBox StaticDescriptionTextBox;
        private System.Windows.Forms.Label StaticDescriptionLabel;
        private System.Windows.Forms.Label BackgroundImageLabel;
        private System.Windows.Forms.Label StreamerAvatarImageLabel;
        private System.Windows.Forms.Label StreamerAvatarImageStatusLabel;
        private System.Windows.Forms.Button BrowseStreamerAvatarImageButton;
        private System.Windows.Forms.Label GameLogoImageLabel;
        private System.Windows.Forms.Label GameLogoImageStatusLabel;
        private System.Windows.Forms.Button BrowseGameLogoImageButton;
        private System.Windows.Forms.CheckBox BackgroundImageCheckBox;
        private System.Windows.Forms.CheckBox GameLogoImageCheckBox;
        private System.Windows.Forms.CheckBox StreamerAvatarImageCheckBox;
        private System.Windows.Forms.Button CountdownDescriptionFontConfigButton;
        private System.Windows.Forms.Button CountdownTimerFontConfigButton;
        private System.Windows.Forms.Button StaticDescriptionFontConfigButton;
    }
}