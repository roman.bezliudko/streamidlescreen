﻿using StreamIdleScreen.Parameters;
using System;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;

namespace StreamIdleScreen
{
    public partial class FontConfigForm : Form
    {
        private DrawnTextParams drawnTextParams;

        private string pickedCustomFontName;

        private readonly string INVALID_FONT_FILE = "Не вдалось завантажити шрифт.";
        private readonly string CUSTOM_FONT_NOT_FOUND = "Шрифт не встановлено.";

        public FontConfigForm(DrawnTextParams drawnTextParams)
        {
            InitializeComponent();
            this.drawnTextParams = drawnTextParams;
            pickedCustomFontName = "";
            FontProvider.FillComboBoxWithSystemFontNames(SystemFontComboBox);
            FontProvider.FillComboBoxWithDefaultFontSizes(FontSizeComboBox);
        }

        private void SystemFontRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            applyFontRadioButtonState();
        }

        private void CustomFontRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            applyFontRadioButtonState();
        }

        private void applyFontRadioButtonState()
        {
            if (SystemFontRadioButton.Checked)
            {
                SystemFontComboBox.Enabled = true;
                BrowseCustomFontButton.Enabled = false;
                CustomFontNameLabel.Visible = false;
            }
            if (CustomFontRadioButton.Checked)
            {
                SystemFontComboBox.Enabled = false;
                BrowseCustomFontButton.Enabled = true;
                CustomFontNameLabel.Visible = true;
            }
        }

        private void BrowseCustomFontButton_Click(object sender, EventArgs e)
        {
            DialogResult result = FontOpenFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string fontFileName = FontOpenFileDialog.FileName;
                try
                {
                    pickedCustomFontName = getFontName(fontFileName);
                    FontProvider.AddCustomFontFromFile(fontFileName);
                    CustomFontNameLabel.Text = pickedCustomFontName;
                }
                catch(Exception)
                {
                    MessageBox.Show(INVALID_FONT_FILE);
                }
            }
        }

        private string getFontName(string fontFileName)
        {
            PrivateFontCollection privateFontCollection = new PrivateFontCollection();
            privateFontCollection.AddFontFile(fontFileName);
            new Font(privateFontCollection.Families[0], 48, FontStyle.Regular);
            // Якщо спроба створити шрифт на основі файлу викликає виключення, то воно буде оброблене ззовні
            return privateFontCollection.Families[0].Name;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            int fontSize = Convert.ToInt32(FontSizeComboBox.Text);

            bool isBold = BoldCheckBox.Checked;
            bool isItalic = ItalicCheckBox.Checked;
            FontStyle finalFontStyle = (FontStyle) ((Convert.ToInt32(isBold) * (int) FontStyle.Bold) | (Convert.ToInt32(isItalic) * (int) FontStyle.Italic));

            if (SystemFontRadioButton.Checked)
            {
                drawnTextParams.Font = new Font(FontProvider.GetSystemFont(SystemFontComboBox.SelectedItem.ToString()), fontSize, finalFontStyle);
            }
            else if (pickedCustomFontName != "")
            {
                FontFamily fontFamily = FontProvider.GetCustomFont(pickedCustomFontName);
                drawnTextParams.Font = new Font(fontFamily, fontSize, finalFontStyle);
            }
            else
            {
                MessageBox.Show(CUSTOM_FONT_NOT_FOUND);
                return;
            }
            Close();
        }
    }
}
