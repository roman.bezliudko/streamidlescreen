﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

namespace StreamIdleScreen
{
    /// <summary>
    /// 
    /// В основу цього класу ліг алгоритм, вкрадений звідси:
    /// https://www.codeproject.com/Articles/13444/Creating-fancy-text-effects-with-C
    /// </summary>
    class TextBeautifier
    {
        private int blurAmount = 6;
        private string text;
        private Font font;
        private Color foreColor;
        private Color backColor;

        public class TextBeautifierBuilder
        {
            private int blurAmount = 6;
            private string text;
            private Font font;
            private Color foreColor;
            private Color backColor;

            public TextBeautifierBuilder SetBlurAmount(Int32 blurAmount)
            {
                this.blurAmount = blurAmount;
                return this;
            }

            public TextBeautifierBuilder SetText(string text)
            {
                this.text = text;
                return this;
            }

            public TextBeautifierBuilder SetFont(Font font)
            {
                this.font = font;
                return this;
            }

            public TextBeautifierBuilder SetForeColor(Color foreColor)
            {
                this.foreColor = foreColor;
                return this;
            }

            public TextBeautifierBuilder SetBackColor(Color backColor)
            {
                this.backColor = backColor;
                return this;
            }

            public Image Build()
            {
                TextBeautifier localTextBeautifier = new TextBeautifier();
                localTextBeautifier.blurAmount = this.blurAmount;
                localTextBeautifier.text = this.text;
                localTextBeautifier.font = this.font;
                localTextBeautifier.foreColor = this.foreColor;
                localTextBeautifier.backColor = this.backColor;
                return localTextBeautifier.MakeImageFromText();
            }
        }

        private TextBeautifier()
        {

        }

        public Image MakeImageFromText()
        {
            Bitmap bmpOut = null; // bitmap we are creating and will return from this function.

            using (Graphics g = Graphics.FromHwnd(IntPtr.Zero))
            {
                SizeF sz = g.MeasureString(text, font);
                using (Bitmap bmp = new Bitmap((int)sz.Width, (int)sz.Height))
                using (Graphics gBmp = Graphics.FromImage(bmp))
                using (SolidBrush brBack = new SolidBrush(Color.FromArgb(16, backColor.R, backColor.G, backColor.B)))
                using (SolidBrush brFore = new SolidBrush(foreColor))
                {
                    gBmp.SmoothingMode = SmoothingMode.HighQuality;
                    gBmp.InterpolationMode = InterpolationMode.HighQualityBilinear;
                    gBmp.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                    gBmp.DrawString(text, font, brBack, 0, 0);

                    // make bitmap we will return.
                    bmpOut = new Bitmap(bmp.Width + blurAmount, bmp.Height + blurAmount);
                    using (Graphics gBmpOut = Graphics.FromImage(bmpOut))
                    {
                        gBmpOut.SmoothingMode = SmoothingMode.HighQuality;
                        gBmpOut.InterpolationMode = InterpolationMode.HighQualityBilinear;
                        gBmpOut.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                        // smear image of background of text about to make blurred background "halo"
                        for (int x = 0; x <= blurAmount; x++)
                            for (int y = 0; y <= blurAmount; y++)
                                gBmpOut.DrawImageUnscaled(bmp, x, y);

                        // draw actual text
                        gBmpOut.DrawString(text, font, brFore, blurAmount / 2, blurAmount / 2);
                    }
                }
            }

            return bmpOut;
        }
    }
}
