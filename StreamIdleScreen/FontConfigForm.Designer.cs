﻿namespace StreamIdleScreen
{
    partial class FontConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SystemFontComboBox = new System.Windows.Forms.ComboBox();
            this.CustomFontNameLabel = new System.Windows.Forms.Label();
            this.CustomFontRadioButton = new System.Windows.Forms.RadioButton();
            this.SystemFontRadioButton = new System.Windows.Forms.RadioButton();
            this.BrowseCustomFontButton = new System.Windows.Forms.Button();
            this.FontSizeComboBox = new System.Windows.Forms.ComboBox();
            this.FontSizeLabel = new System.Windows.Forms.Label();
            this.BoldCheckBox = new System.Windows.Forms.CheckBox();
            this.ItalicCheckBox = new System.Windows.Forms.CheckBox();
            this.CancelButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.FontOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // SystemFontComboBox
            // 
            this.SystemFontComboBox.FormattingEnabled = true;
            this.SystemFontComboBox.Location = new System.Drawing.Point(121, 28);
            this.SystemFontComboBox.Name = "SystemFontComboBox";
            this.SystemFontComboBox.Size = new System.Drawing.Size(226, 21);
            this.SystemFontComboBox.TabIndex = 16;
            // 
            // CustomFontNameLabel
            // 
            this.CustomFontNameLabel.AutoSize = true;
            this.CustomFontNameLabel.Location = new System.Drawing.Point(217, 71);
            this.CustomFontNameLabel.Name = "CustomFontNameLabel";
            this.CustomFontNameLabel.Size = new System.Drawing.Size(0, 13);
            this.CustomFontNameLabel.TabIndex = 15;
            this.CustomFontNameLabel.Visible = false;
            // 
            // CustomFontRadioButton
            // 
            this.CustomFontRadioButton.AutoSize = true;
            this.CustomFontRadioButton.Location = new System.Drawing.Point(21, 69);
            this.CustomFontRadioButton.Name = "CustomFontRadioButton";
            this.CustomFontRadioButton.Size = new System.Drawing.Size(74, 17);
            this.CustomFontRadioButton.TabIndex = 14;
            this.CustomFontRadioButton.Text = "Зовнішній";
            this.CustomFontRadioButton.UseVisualStyleBackColor = true;
            this.CustomFontRadioButton.CheckedChanged += new System.EventHandler(this.CustomFontRadioButton_CheckedChanged);
            // 
            // SystemFontRadioButton
            // 
            this.SystemFontRadioButton.AutoSize = true;
            this.SystemFontRadioButton.Checked = true;
            this.SystemFontRadioButton.Location = new System.Drawing.Point(21, 29);
            this.SystemFontRadioButton.Name = "SystemFontRadioButton";
            this.SystemFontRadioButton.Size = new System.Drawing.Size(90, 17);
            this.SystemFontRadioButton.TabIndex = 13;
            this.SystemFontRadioButton.TabStop = true;
            this.SystemFontRadioButton.Text = "Стандартний";
            this.SystemFontRadioButton.UseVisualStyleBackColor = true;
            this.SystemFontRadioButton.CheckedChanged += new System.EventHandler(this.SystemFontRadioButton_CheckedChanged);
            // 
            // BrowseCustomFontButton
            // 
            this.BrowseCustomFontButton.Enabled = false;
            this.BrowseCustomFontButton.Location = new System.Drawing.Point(121, 66);
            this.BrowseCustomFontButton.Name = "BrowseCustomFontButton";
            this.BrowseCustomFontButton.Size = new System.Drawing.Size(72, 23);
            this.BrowseCustomFontButton.TabIndex = 12;
            this.BrowseCustomFontButton.Text = "Вказати...";
            this.BrowseCustomFontButton.UseVisualStyleBackColor = true;
            this.BrowseCustomFontButton.Click += new System.EventHandler(this.BrowseCustomFontButton_Click);
            // 
            // FontSizeComboBox
            // 
            this.FontSizeComboBox.FormattingEnabled = true;
            this.FontSizeComboBox.Location = new System.Drawing.Point(80, 127);
            this.FontSizeComboBox.Name = "FontSizeComboBox";
            this.FontSizeComboBox.Size = new System.Drawing.Size(47, 21);
            this.FontSizeComboBox.TabIndex = 17;
            // 
            // FontSizeLabel
            // 
            this.FontSizeLabel.AutoSize = true;
            this.FontSizeLabel.Location = new System.Drawing.Point(24, 130);
            this.FontSizeLabel.Name = "FontSizeLabel";
            this.FontSizeLabel.Size = new System.Drawing.Size(42, 13);
            this.FontSizeLabel.TabIndex = 18;
            this.FontSizeLabel.Text = "Розмір";
            // 
            // BoldCheckBox
            // 
            this.BoldCheckBox.AutoSize = true;
            this.BoldCheckBox.Location = new System.Drawing.Point(166, 129);
            this.BoldCheckBox.Name = "BoldCheckBox";
            this.BoldCheckBox.Size = new System.Drawing.Size(67, 17);
            this.BoldCheckBox.TabIndex = 19;
            this.BoldCheckBox.Text = "Жирний";
            this.BoldCheckBox.UseVisualStyleBackColor = true;
            // 
            // ItalicCheckBox
            // 
            this.ItalicCheckBox.AutoSize = true;
            this.ItalicCheckBox.Location = new System.Drawing.Point(258, 129);
            this.ItalicCheckBox.Name = "ItalicCheckBox";
            this.ItalicCheckBox.Size = new System.Drawing.Size(62, 17);
            this.ItalicCheckBox.TabIndex = 20;
            this.ItalicCheckBox.Text = "Курсив";
            this.ItalicCheckBox.UseVisualStyleBackColor = true;
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(179, 168);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 21;
            this.CancelButton.Text = "Скасувати";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(272, 168);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 22;
            this.SaveButton.Text = "Зберегти";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // FontOpenFileDialog
            // 
            this.FontOpenFileDialog.FileName = "openFileDialog1";
            // 
            // FontConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 214);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ItalicCheckBox);
            this.Controls.Add(this.BoldCheckBox);
            this.Controls.Add(this.FontSizeLabel);
            this.Controls.Add(this.FontSizeComboBox);
            this.Controls.Add(this.SystemFontComboBox);
            this.Controls.Add(this.CustomFontNameLabel);
            this.Controls.Add(this.CustomFontRadioButton);
            this.Controls.Add(this.SystemFontRadioButton);
            this.Controls.Add(this.BrowseCustomFontButton);
            this.Name = "FontConfigForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Шрифт";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox SystemFontComboBox;
        private System.Windows.Forms.Label CustomFontNameLabel;
        private System.Windows.Forms.RadioButton CustomFontRadioButton;
        private System.Windows.Forms.RadioButton SystemFontRadioButton;
        private System.Windows.Forms.Button BrowseCustomFontButton;
        private System.Windows.Forms.ComboBox FontSizeComboBox;
        private System.Windows.Forms.Label FontSizeLabel;
        private System.Windows.Forms.CheckBox BoldCheckBox;
        private System.Windows.Forms.CheckBox ItalicCheckBox;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.OpenFileDialog FontOpenFileDialog;
    }
}