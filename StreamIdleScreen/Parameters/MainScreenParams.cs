﻿using System.Drawing.Text;

namespace StreamIdleScreen.Parameters
{
    public class MainScreenParams
    {
        public DrawnStaticTextParams CountdownDescriptionParams { get; set; }
        public DrawnTimerTextParams CountdownTimerParams { get; set; }
        public DrawnStaticTextParams StaticDescriptionParams { get; set; }

        public ImageParams BackgroundImageParams { get; set; }
        public ImageParams GameLogoImageParams { get; set; }
        public ImageParams StreamerAvatarImageParams { get; set; }
    }
}
