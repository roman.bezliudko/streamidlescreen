﻿using System;

namespace StreamIdleScreen.Parameters
{
    public class DrawnTimerTextParams : DrawnTextParams
    {
        private readonly TimeSpan ONE_SECOND_TIMEDIFF = new TimeSpan(0, 0, 1);

        public TimeSpan Time { get; private set; }
        
        public void SetTimeInMinutes(int minutes)
        {
            Time = new TimeSpan(0, minutes, 0);
        }

        public void OneSecondTick()
        {
            Time -= ONE_SECOND_TIMEDIFF;
        }
    }
}
