﻿using System.Drawing;

namespace StreamIdleScreen.Parameters
{
    public class DrawnTextParams
    {
        public Font Font { get; set; }
        public Color ForeColor { get; set; }
        public Color BackColor { get; set; }
        public int ContourThickness { get; set; }
    }
}
