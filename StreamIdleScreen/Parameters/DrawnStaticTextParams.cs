﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamIdleScreen.Parameters
{
    public class DrawnStaticTextParams : DrawnTextParams
    {
        public string Text { get; set; }
    }
}
