﻿namespace StreamIdleScreen.Parameters
{
    public class ImageParams
    {
        public string ImagePath { get; set; }
        public bool Disabled { get; set; }
        
        public ImageParams()
        {
            ImagePath = "";
            Disabled = false;
        }
    }
}
