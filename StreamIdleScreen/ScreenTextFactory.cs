﻿using StreamIdleScreen.Parameters;
using System.Drawing;

namespace StreamIdleScreen
{
    public class ScreenTextFactory
    {

        public static Image MakeStaticTextImage(DrawnStaticTextParams staticTextParams)
        {
            return MakeTextImage(staticTextParams, staticTextParams.Text);
        }

        public static Image MakeTimerTextImage(DrawnTimerTextParams timerTextParams)
        {
            return MakeTextImage(timerTextParams, timerTextParams.Time.ToString(@"mm\:ss"));
        }

        private static Image MakeTextImage(DrawnTextParams textParams, string text)
        {
            return new TextBeautifier.TextBeautifierBuilder()
                .SetBlurAmount(textParams.ContourThickness)
                .SetText(text)
                .SetFont(textParams.Font)
                .SetForeColor(textParams.ForeColor)
                .SetBackColor(textParams.BackColor)
                .Build();
        }
    }
}
