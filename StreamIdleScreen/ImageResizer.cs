﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace StreamIdleScreen
{
    /// <summary>
    /// Код для цього класу вкрадено звідси:
    /// 
    /// </summary>
    class ImageResizer
    {
        public Image FitImageByContainerSize(Image image, int containerWidth, int containerHeight)
        {
            // Найкращий сценарій: зображення повністю вписується в контейнер і не потребує стиснення
            if (image.Width <= containerWidth && image.Height <= containerHeight)
            {
                return image;
            }
            // Зображення вписується за висотою, але не вписується за шириною
            // Здійснюється стиснення зображення до ширини контейнера зі збереженням пропорцій
            else if (image.Width > containerWidth && image.Height <= containerHeight)
            {
                double widthRatio = (double)containerWidth / image.Width;
                int proportionalHeight = Convert.ToInt32(image.Height * widthRatio);
                return ResizeImage(image, containerWidth, proportionalHeight);
            }
            // Зображення вписується за шириною, але не вписується за висотою
            // Здійснюється стиснення зображення до висоти контейнера зі збереженням пропорцій
            else if (image.Width <= containerWidth && image.Height > containerHeight)
            {
                double heightRatio = (double)containerHeight / image.Height;
                int proportionalWidth = Convert.ToInt32(image.Width * heightRatio);
                return ResizeImage(image, proportionalWidth, containerHeight);
            }
            // Найгірший сценарій: зображення не вписується ні за шириною, ні за висотою
            // В такому випадку розмір зображення підганяється під той параметр, який сильніше розходиться з параметром контейнера
            else
            {
                double widthRatio = (double)containerWidth / image.Width;
                double heightRatio = (double)containerHeight / image.Height;
                if (widthRatio < heightRatio)
                {
                    int proportionalWidth = Convert.ToInt32(image.Width * widthRatio);
                    int proportionalHeight = Convert.ToInt32(image.Height * widthRatio);
                    return ResizeImage(image, proportionalWidth, proportionalHeight);
                }
                else
                {
                    int proportionalWidth = Convert.ToInt32(image.Width * heightRatio);
                    int proportionalHeight = Convert.ToInt32(image.Height * heightRatio);
                    return ResizeImage(image, proportionalWidth, proportionalHeight);
                }
            }
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        private Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}
