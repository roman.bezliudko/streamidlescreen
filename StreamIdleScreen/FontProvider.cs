﻿using System;
using System.Drawing;
using System.Drawing.Text;

#nullable enable

namespace StreamIdleScreen
{
    class FontProvider
    {
        private static readonly int[] DEFAULT_FONT_SIZE_LIST = new int[] { 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72 };

        private static InstalledFontCollection installedFontCollection;
        private static PrivateFontCollection privateFontCollection;
        
        static FontProvider()
        {
            installedFontCollection = new InstalledFontCollection();
            privateFontCollection = new PrivateFontCollection();
        }

        public static System.Windows.Forms.ComboBox FillComboBoxWithSystemFontNames(System.Windows.Forms.ComboBox comboBox)
        {
            foreach (FontFamily font in installedFontCollection.Families)
            {
                comboBox.Items.Add(font.Name);
            }
            comboBox.SelectedIndex = 0;
            return comboBox;
        }

        public static System.Windows.Forms.ComboBox FillComboBoxWithDefaultFontSizes(System.Windows.Forms.ComboBox comboBox)
        {
            foreach (int fontSize in DEFAULT_FONT_SIZE_LIST)
            {
                comboBox.Items.Add(fontSize);
            }
            // For this size list index 14 will match the font size 48
            comboBox.SelectedIndex = 14;
            return comboBox;
        }

        public static FontFamily? GetSystemFont(string fontName)
        {
            return GetFontByName(fontName, installedFontCollection);
        }

        public static FontFamily? GetCustomFont(string fontName)
        {
            return GetFontByName(fontName, privateFontCollection);
        }

        private static FontFamily? GetFontByName(string fontName, FontCollection fontCollection)
        {
            if (Array.Exists(fontCollection.Families, font => font.Name == fontName))
            {
                return Array.Find(fontCollection.Families, font => font.Name == fontName);
            }
            else
            {
                return null;
            }
        }

        public static void AddCustomFontFromFile(string fontFileName)
        {
            privateFontCollection.AddFontFile(fontFileName);
        }
    }
}
