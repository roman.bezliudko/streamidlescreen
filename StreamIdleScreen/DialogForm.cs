﻿using StreamIdleScreen.Parameters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;

namespace StreamIdleScreen
{
    public partial class DialogForm : Form
    {
        private readonly string IMAGE_LOADING_FAILED = "Не вдається завантажити зображення.";

        private readonly string BACKGROUND_IMAGE_NOT_FOUND = "Фонове зображення не встановлено.";
        private readonly string GAME_LOGO_IMAGE_NOT_FOUND = "Логотип гри не встановлено.";
        private readonly string STREAMER_AVATAR_IMAGE_NOT_FOUND = "Аватар стрімера не встановлено.";

        private MainScreenParams mainScreenParams;

        public DialogForm()
        {
            InitializeComponent();

            mainScreenParams = new MainScreenParams();
            mainScreenParams.BackgroundImageParams = new ImageParams();
            mainScreenParams.GameLogoImageParams = new ImageParams();
            mainScreenParams.StreamerAvatarImageParams = new ImageParams();
            mainScreenParams.CountdownDescriptionParams = new DrawnStaticTextParams();
            mainScreenParams.CountdownTimerParams = new DrawnTimerTextParams();
            mainScreenParams.StaticDescriptionParams = new DrawnStaticTextParams();
        }

        private void BrowseBackgroundImageButton_Click(object sender, EventArgs e)
        {
            loadImageFromFile(mainScreenParams.BackgroundImageParams, BackgroundImageStatusLabel);
        }

        private void BrowseGameLogoImageButton_Click(object sender, EventArgs e)
        {
            loadImageFromFile(mainScreenParams.GameLogoImageParams, GameLogoImageStatusLabel);
        }

        private void BrowseStreamerAvatarImageButton_Click(object sender, EventArgs e)
        {
            loadImageFromFile(mainScreenParams.StreamerAvatarImageParams, StreamerAvatarImageStatusLabel);
        }

        private void loadImageFromFile(ImageParams imageParams, Label statusLabel)
        {
            DialogResult result = ResourceOpenFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string fileName = ResourceOpenFileDialog.FileName;
                try
                {
                    ImagePreviewPictureBox.Image = Image.FromFile(fileName);
                    // Якщо до цього моменту не злетить, то далі можна зберегти шлях до файлу
                    imageParams.ImagePath = fileName;
                    statusLabel.Text = fileName;
                }
                catch (Exception)
                {
                    MessageBox.Show(IMAGE_LOADING_FAILED);
                }
            }
        }

        private void CountdownTrackbar_Scroll(object sender, EventArgs e)
        {
            CountdownTimerTrackbarValueLabel.Text = CountdownTimerTrackbar.Value.ToString();
        }

        private void BackgroundImageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            mainScreenParams.BackgroundImageParams.Disabled = ((CheckBox)sender).Checked;
            BrowseBackgroundImageButton.Enabled = !mainScreenParams.BackgroundImageParams.Disabled;
        }

        private void GameLogoImageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            mainScreenParams.GameLogoImageParams.Disabled = ((CheckBox)sender).Checked;
            BrowseGameLogoImageButton.Enabled = !mainScreenParams.GameLogoImageParams.Disabled;
        }

        private void StreamerAvatarImageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            mainScreenParams.StreamerAvatarImageParams.Disabled = ((CheckBox)sender).Checked;
            BrowseStreamerAvatarImageButton.Enabled = !mainScreenParams.StreamerAvatarImageParams.Disabled;
        }

        private void CountdownDescriptionFontConfigButton_Click(object sender, EventArgs e)
        {
            using (FontConfigForm fontConfigForm = new FontConfigForm(mainScreenParams.CountdownDescriptionParams))
            {
                fontConfigForm.ShowDialog(this);
            }
        }

        private void CountdownTimerFontConfigButton_Click(object sender, EventArgs e)
        {
            using (FontConfigForm fontConfigForm = new FontConfigForm(mainScreenParams.CountdownTimerParams))
            {
                fontConfigForm.ShowDialog(this);
            }
        }

        private void StaticDescriptionFontConfigButton_Click(object sender, EventArgs e)
        {
            using (FontConfigForm fontConfigForm = new FontConfigForm(mainScreenParams.StaticDescriptionParams))
            {
                fontConfigForm.ShowDialog(this);
            }
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(mainScreenParams.BackgroundImageParams.ImagePath) && !mainScreenParams.BackgroundImageParams.Disabled)
            {
                MessageBox.Show(BACKGROUND_IMAGE_NOT_FOUND);
                return;
            }
            if (String.IsNullOrEmpty(mainScreenParams.GameLogoImageParams.ImagePath) && !mainScreenParams.GameLogoImageParams.Disabled)
            {
                MessageBox.Show(GAME_LOGO_IMAGE_NOT_FOUND);
                return;
            }
            if (String.IsNullOrEmpty(mainScreenParams.StreamerAvatarImageParams.ImagePath) && !mainScreenParams.StreamerAvatarImageParams.Disabled)
            {
                MessageBox.Show(STREAMER_AVATAR_IMAGE_NOT_FOUND);
                return;
            }

            mainScreenParams.CountdownDescriptionParams.Text = CountdownDescriptionTextBox.Text;
            mainScreenParams.StaticDescriptionParams.Text = StaticDescriptionTextBox.Text;
            mainScreenParams.CountdownTimerParams.SetTimeInMinutes(CountdownTimerTrackbar.Value);

            // TODO: Наразі ці параметри хардкордяться, в майбутніх версіях треба уможливити і їх задання теж
            mainScreenParams.CountdownDescriptionParams.BackColor = Color.Black;
            mainScreenParams.CountdownDescriptionParams.ForeColor = Color.LightGreen;
            mainScreenParams.CountdownDescriptionParams.ContourThickness = 7;
            mainScreenParams.CountdownTimerParams.BackColor = Color.Black;
            mainScreenParams.CountdownTimerParams.ForeColor = Color.LightGreen;
            mainScreenParams.CountdownTimerParams.ContourThickness = 7;
            mainScreenParams.StaticDescriptionParams.BackColor = Color.Black;
            mainScreenParams.StaticDescriptionParams.ForeColor = Color.LightGreen;
            mainScreenParams.StaticDescriptionParams.ContourThickness = 7;

            new MainScreenForm(mainScreenParams).Show();
            Hide();
        }
    }
}
