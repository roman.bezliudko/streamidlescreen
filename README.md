# StreamIdleScreen

Ця невеличка програмка допомагає в оформленні та демонстрації екрану очікування для ігрових стрімів на Twitch.

## Особливості актуальної версії

### Що вже можна задавати

* Текст, що відображається над таймером
* Текст, що відображається після закінчення таймера
* Час зворотнього відліку (в хвилинах)
* Фонове зображення
* Логотип гри (вгорі по центру)
* Аватару стрімера (нижній правий кут)
* Шрифт тексту (один із стандартних або зовнішній)
* Розмір шрифта та додаткові атрибути (жирний, курсив)

### Що задавати ще не можна, але планується уможливити в майбутніх версіях

* Додаткові параметри для тексту (колір, товщина контуру)
* Фонова музика
    * Статична музика
    * Музичний плейлист
    * Музика при завершенні таймера
    * Музичний візуалізатор
* Кеш для використаних зображень, шрифтів та звуків
* Користувацькі профілі для збереження налаштувань

## Історія змін

### v0.5.0
* Додано можливість встановлювати шрифт індивідуально для кожного з написів.
    * Для регулювання шрифта виділено окреме вікно.
    * Окрім самого шрифта також можна встановлювати розмір тексту та додаткові атрибути (жирний, курсив).
    * Колір та контур шрифта все ще не підлягають налаштуванню.
* Рефакторинг: виділено окремі класи для атрибутів зображень (шлях до картинки, режим (ввімк./вимк.)) та текстів (шрифт, колір, контур, текст/час).

### v0.4.0
* Додано галочки для ввімкнення/вимкнення зображень для фону, логотипу гри та аватари стрімера.
* Додано можливість встановлювати шрифт тексту.
    * Шрифт може бути встановлений як один зі стандартних, так і зовнішній.
    * Ефекти для шрифтів (розмір, колір, контур) все ще не підлягають налаштуванню.
* Додано валідацію для запобігання запуску екрану очікування без встановлених зображень чи шрифта.
* Вікно екрану очікування зроблено безмежним (borderless).
* Аватара стрімера тепер правильно зсовується в нижній правий кут екрану за будь-якого розміру.
* Натиснення на аватару миттєво зупиняє таймер.
* Рефакторинг: для параметрів екрану очікування було виділено окремий клас.

### v0.3.0
* На екран очікування додано контейнери для логотипу гри та аватари стрімера.
* Логотип гри та аватара стрімера підлягають конфігурації через діалогову форму.
* За поточної реалізації логотип гри розташовується у контейнері статичного розміру вгорі екрану. Якщо розмір зображення не вписується, то зображення стискається зі збереженням пропорцій.
* Аналогічним чином аватар розміщується в квадратному контейнері 200x200 в нижньому правому куті екрану.

### v0.2.0
* Компоненти Label з вікна екрану очікування викинуто на мороз.
* Замість них тепер текст відображають алгоритмічно побудовані зображення в компонентах PictureBox.
* Текст в цих зображеннях перемальовується з наданням йому чорного контура — тепер програма на один крок ближче до введення цього функціоналу з можливістю регулювання через інтерфейс.
* В силу цих змін було проведено невеличкий рефакторинг: створено два нові класи, трохи структуровано код в класі форми екрану очікування.
* Фонове зображення, що стояло за замовчуванням, видалено з ресурсів.

### v0.1.2
* Підрихтовано вкладені списки в README.md.

### v0.1.1
* Додано файл README.md (ага, цей самий, що ви зараз читаєте).

### v0.1.0
* Програму створено. Єй!
* Виділено два вікна — вікно діалогу (для задання параметрів) та, власне, вікно екрану очікування.
* У вікні діалогу дозволено встановлювати наступні параметри:
    * Шлях до фонового зображення.
    * Текст, що відображається над таймером.
    * Текст, що відображається після закінчення таймера.
    * Час зворотнього відліку в хвилинах, від 1 до 30.